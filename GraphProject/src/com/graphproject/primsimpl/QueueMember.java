/**
 * @author Daniel DeCola
 * @email ddecola@gmail.com
 */
package com.graphproject.primsimpl;

import java.util.Comparator;

public class QueueMember {
	public Vertex data;
	public Integer priority;

	/**
	 * Wrapper class for outgoing Vertex and edge weight to be used by the PriorityQueue
	 */
	public QueueMember(Vertex data, Integer priority) {
		this.data = data;
		this.priority = priority;
	}

	public Integer getPriority() {
		return priority;
	}

	public Vertex getVertex() {
		return data;
	}

	public void setPriority(Integer pri) {
		this.priority = pri;
	}

	public boolean equals(QueueMember mem) {
		return mem.getVertex().getId().equals(getVertex().getId()) && mem.getPriority() == getPriority();
	}

	public static class QueCompare implements Comparator<QueueMember> {
		@Override
		public int compare(QueueMember a, QueueMember b) {
			if (a == null)
				return 1;
			if (b == null)
				return -1;
			if (a.equals(b) || a.getPriority() == b.getPriority())
				return 0;
			if (a.getPriority() < b.getPriority())
				return -1;
			else // b > a
				return 1;
		}
	}
}