/**
 * @author Daniel DeCola
 * @email ddecola@gmail.com
 */
package com.graphproject.primsimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *  an undirected, weighted graph
 */
public class WeightedGraph {

	/* Maps vertex to adjacency map of vertex and edge weight */
	private final Map<Vertex, Map<Vertex, Integer>> graphMap;

	public WeightedGraph() {
		graphMap = new HashMap<Vertex, Map<Vertex, Integer>>();
	}

	/**
	 * adds a given vertex to the graph
	 *
	 * @param a the vertex to be added
	 * @return false vertex was already in graph, true otherwise.
	 */
	public boolean addVertex(Vertex a) {
		if (graphMap.containsKey(a))
			return false;

		/* adds vertex to map and initializes empty adjacency map of <end vertex, weight> */
		graphMap.put(a, new HashMap<Vertex, Integer>());
		return true;
	}

	/**
	 * removes the vertex from the graph
	 *
	 * @param a the vertex to be removed
	 */
	public void removeVertex(Vertex a) {
		/* for each outgoing edge in target vertex's edge adjacency map, remove the entry for the <end vertex, weight> of 'a' */
		for (Vertex endVertex  : graphMap.get(a).keySet()) {
			graphMap.get(endVertex).remove(a);
		}
		/* then remove target vertex from graph itself */
		graphMap.remove(a);
	}

	/**
	 * adds an edge to the graph between {@code a} and {@code b} with {@code weight}
	 *
	 * Note: start and end are used loosely, this is an undirected graph!
	 * @param a start vertex of edge
	 * @param b end vertex of edge
	 * @param weight edge weight
	 * @throws Exception
	 */
	public void addEdge(Vertex a, Vertex b, int weight) throws Exception {
		if (!graphMap.containsKey(a) || !graphMap.containsKey(b))
			throw new Exception(String.format("Attempted to add edge between vertex %s and %s not in graph.", a.getId(), b.getId()));

		/* both vertex a/b need to be aware of edge, add end vertex to adjacency map */
		graphMap.get(a).put(b, weight);
		graphMap.get(b).put(a, weight);
	}

	/**
	 * removes the edge between vertices a and b
	 *
	 * @param a start vertex of edge
	 * @param b end vertex of edge
	 * @throws Exception
	 */
	public void removeEdge(Vertex a, Vertex b) throws Exception {
		if (!graphMap.containsKey(a) || !graphMap.containsKey(b))
			throw new Exception(String.format("Attempted to remove edge between vertex %s and %s not in graph.", a.getId(), b.getId()));

		if (!graphMap.get(a).containsKey(b) || !graphMap.get(b).containsKey(a))
			throw new Exception(String.format("Edge does not exist between %s and %s", a.getId(), b.getId()));

		/* remove both 'ends' of the edge from the graph by deleting <end vertex, weight> entry for each vertex */
		graphMap.get(a).remove(b);
		graphMap.get(b).remove(a);
	}

	public Integer edgeWeight(Vertex a, Vertex b) throws Exception {
		if (!graphMap.containsKey(a) || !graphMap.containsKey(b))
			throw new Exception(String.format("Attempted to get edge weight with vertex %s and %s not in graph.", a.getId(), b.getId()));

		if (!graphMap.get(a).containsKey(b) || !graphMap.get(b).containsKey(a))
			throw new Exception(String.format("Edge does not exist between %s and %s", a.getId(), b.getId()));

		return graphMap.get(a).get(b);
	}

	/**
	 * @param a start vertex
	 * @return Map<Vertex, Integer> containing all end vertices and edge weights from a given vertex a
	 * @throws Exception
	 */
	public Map<Vertex, Integer> getAdjacentEdges(Vertex a) throws Exception {
		if (!graphMap.containsKey(a))
			throw new Exception(String.format("Attempted to get adjacent edges of vertex %s not in graph.", a.getId()));
		/* since hashmap give no definite ordering */
		return Collections.unmodifiableMap(graphMap.get(a));
	}

	/**
	 * @return the aggregate of all edge weights in the graph
	 */
	public int getGraphWeight() {
		Integer graphWeight = 0;
		for (Map<Vertex, Integer> edges : graphMap.values()) {
			for (Integer weight : edges.values()) {
				graphWeight += weight;
			}
		}
		/* graphMap contains entries for both directions: <A, B,4>> and <B, <A,4>>
		 * so we interate over the entire list and then divide by two since every edge weight is recorded twice*/
		return graphWeight/2;
	}

	/**
	 * @return entire set of startvertex - endvertex - weight, this will output both directions edge directions B -> A, A -> B
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();

		for (Map.Entry<Vertex, Map<Vertex, Integer>> entry : graphMap.entrySet()) {
			/* start vertex if no end vertices */
			if (entry.getValue().isEmpty())
				str.append(entry.getKey().toString()).append("\r");

			/* [start vertex]	[end vertex]	[weight] */
			for (Map.Entry<Vertex, Integer> innerEntry : entry.getValue().entrySet()) {
				str.append(entry.getKey().toString()).append("\t").append(innerEntry.getKey().toString()).append("\t").append(innerEntry.getValue()).append("\r");
			}
		}
		return str.toString();
	}

	/**
	 * if the graph contains the edge: A->B, it will only output A B [weight] OR B A [weight]
	 * used to show resulting MST from prim algorithm
	 *
	 * @return graph as [start vertex]	[end vertex]	[weight], with only 1 edge entry/direction per vertex
	 */
	public ArrayList<String> showGraph() {
		final ArrayList<String>	list = new ArrayList<String>();
		StringBuilder str = new StringBuilder();
		final List<Vertex> alreadyOutput = new LinkedList<Vertex>();

		for (Map.Entry<Vertex, Map<Vertex, Integer>> entry : graphMap.entrySet()) {
			/* start vertex if no end vertices */
			if (entry.getValue().isEmpty()) {
				list.add(str.append(entry.getKey().toString()).append("\t").toString());
				str = new StringBuilder();
			}

			alreadyOutput.add(entry.getKey());

			/* [start vertex]	[end vertex]	[weight] */
			for (Map.Entry<Vertex, Integer> innerEntry : entry.getValue().entrySet()) {

				if (alreadyOutput.contains(innerEntry.getKey()))
					continue;
				/* add each edge to the list, makes for easier printing to file */
				list.add(str.append(entry.getKey().toString()).append("\t").append(innerEntry.getKey().toString()).append("\t").append(innerEntry.getValue()).toString());
				str = new StringBuilder();
			}
		}
		return list;
	}

	/**
	 * used when adding initial edges from input file
	 *
	 * @param vert string identifier of vertex
	 * @return vertex id'd by vert
	 */
	public Vertex getVertex(String vert) {
		for (Vertex v : graphMap.keySet()) {
			if (v.getId().equals(vert))
				return v;
		}
		return null;
	}

	/**
	 * @return arbitrarily chosen vertex to start minimum spanning tree with
	 */
	public Vertex getStartingVertex() {
		return graphMap.keySet().iterator().next();
	}

	public boolean hasVertex(Vertex a) {
		return graphMap.containsKey(a);
	}

	public int getNumVertices() {
		return graphMap.size();
	}
}