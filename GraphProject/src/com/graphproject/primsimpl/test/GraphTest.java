/**
 * @author Daniel DeCola
 * @email ddecola@gmail.com
 */
package com.graphproject.primsimpl.test;

import org.junit.Assert;
import org.junit.Test;

import com.graphproject.primsimpl.PrimImpl;
import com.graphproject.primsimpl.Vertex;
import com.graphproject.primsimpl.WeightedGraph;

public class GraphTest {
	private static final Vertex a = new Vertex("A");
	private static final Vertex b = new Vertex("B");
	private static final Vertex c = new Vertex("C");
	private static final Vertex d = new Vertex("D");
	private static final Vertex e = new Vertex("E");

	private static final Vertex arg = new Vertex("arg");
	private static final Vertex blah = new Vertex("blah");
	private static final Vertex the = new Vertex("the");
	private static final Vertex game = new Vertex("game");

	@Test
	public void testPrim() throws Exception {
		final WeightedGraph graph = new WeightedGraph();
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addVertex(e);

		graph.addEdge(a, b, 1);
		graph.addEdge(c, b, 2);
		graph.addEdge(d, a, 5);
		graph.addEdge(a, e, 3);
		graph.addEdge(b, e, 2);

		final WeightedGraph outGraph = PrimImpl.primMST(graph);
		Assert.assertEquals(10, outGraph.getGraphWeight());
	}

	@Test
	public void testPrim2() throws Exception {
		final WeightedGraph graph = new WeightedGraph();
		graph.addVertex(arg);
		graph.addVertex(blah);
		graph.addVertex(the);
		graph.addVertex(game);
		graph.addVertex(a);
		graph.addVertex(d);
		graph.addVertex(e);

		graph.addEdge(arg, blah, 14);
		graph.addEdge(arg, a, 1);
		graph.addEdge(arg, the, 1);
		graph.addEdge(the, game, 99);
		graph.addEdge(arg, game, 3);
		graph.addEdge(e, game, 34);
		graph.addEdge(d, e, 354);
		graph.addEdge(a, blah, 942);
		graph.addEdge(blah, game, 54);

		final WeightedGraph outGraph = PrimImpl.primMST(graph);
		Assert.assertEquals(407, outGraph.getGraphWeight());
	}
}
