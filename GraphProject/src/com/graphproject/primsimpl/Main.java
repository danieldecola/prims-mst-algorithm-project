/**
 * @author Daniel DeCola
 * @email ddecola@gmail.com
 */
package com.graphproject.primsimpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
	/**
	 * Accepts two command line arguments: input file and out file path and
	 * calculates the minimum spanning tree of the graph specified by the input file,
	 * storing the result as [start vertex] [end vertex] [weight] to the output file.
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String inPath = "";
		String outPath = "";
		FileReader fr = null;

		/* all of this console code can be commented out and file paths can be specified
		 * by uncommenting inPath/outPath below */
		Console c = System.console();
		if (c == null) {
			System.err.println("Console access required to input file paths, terminating.");
			System.exit(1);
		}
		inPath = c.readLine("Specify path to input file: ");
		outPath = c.readLine("Specify path to output file (to be created): ");

		if (outPath.equals("")) {
			System.out.println("Please specify output path, terminating.");
			System.exit(1);
		}
		//inPath = "C:/infile.txt";
		//outPath = "C:/outfile.txt";
		try {
			fr = new FileReader(inPath);

			} catch (FileNotFoundException e) {
				System.out.println("Invalid input path, please try again.");
				System.exit(1);
		}

		final WeightedGraph inGraph = new WeightedGraph();
		readInFile(inGraph, fr);

		final WeightedGraph outGraph = PrimImpl.primMST(inGraph);
		writeOutputFile(inGraph, outGraph, outPath);
	}

	/**
	 * Writes out the output graph in the form of:[start vertex]	[end vertex]	[edge weight]
	 * for each edge which makes up the MST.
	 *
	 * @param inGraph initial graph of which the MST was built from.
	 * @param outGraph output graph from the MST algorithm.
	 * @param path output file path
	 * @throws IOException
	 */
	private static void writeOutputFile(WeightedGraph inGraph, WeightedGraph outGraph, String path) throws IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(path));
			final ArrayList<String> outList = outGraph.showGraph();
			final ArrayList<String> inList = inGraph.showGraph();

			writer.write("Prim MST output");
			writer.newLine();
			for (String s : outList) {
				writer.write(s);
				writer.newLine();
			}
			writer.write("Weight of Minimum Spanning Tree is: "+outGraph.getGraphWeight());
			writer.newLine();

			writer.write("Original Graph input");
			writer.newLine();
			for (String s : inList) {
				writer.write(s);
				writer.newLine();
			}
			writer.write("Total input graph weight: "+inGraph.getGraphWeight());
			writer.newLine();
		} finally {
			writer.close();
		}
	}

	/**
	 *
	 * @param graph which gets initialized from the input file
	 * @param fr input file
	 * @throws Exception
	 */
	private static void readInFile(WeightedGraph graph, FileReader fr) throws Exception {
		BufferedReader input = null;
		String currentLine = "";
		try {
			input = new BufferedReader(fr);
			while ((currentLine = input.readLine()) != null) {
				/* edge definitions are comma separated, vertex definitions are one per line */
				if (!currentLine.contains(",")) {
					graph.addVertex(new Vertex(currentLine.trim()));

				} else {
					final Object[] edge = extractEdge(currentLine);
					graph.addEdge(graph.getVertex((String)edge[0]), graph.getVertex((String)edge[1]), (Integer)edge[2]);
				}
			}
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

	/**
	 *  returns an object array in the form: [start vertex, end vertex, edge weight] for easy input to Graph.addEdge
	 *  @param str the line from the input file in the form: A,B,4
	 */
	private static Object[] extractEdge(final String str) {
		/* had to do new String() because of weird offset problems I was encountering that made looking up
		 * keys (vertices) in the graph's map fail. */
		final String startVertex = new String(str.substring(0, str.indexOf(",")));
		final String endVertex = new String(str.substring(str.indexOf(",") + 1, str.lastIndexOf(",")));
		final Integer weight = Integer.valueOf(new String(str.substring(str.lastIndexOf(",") + 1).trim()));

		return new Object[] {startVertex.trim(), endVertex.trim(), weight};
	}
}