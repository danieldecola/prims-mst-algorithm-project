/**
 * @author Daniel DeCola
 * @email ddecola@gmail.com
 */
package com.graphproject.primsimpl;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import com.graphproject.primsimpl.QueueMember.QueCompare;

public class PrimImpl {

	/**
	 * Calculates the Minimum Spanning Tree of the inGraph based on Prim's Algorithm
	 *
	 * @param inGraph
	 * @return
	 * @throws Exception
	 */
	public static WeightedGraph primMST(WeightedGraph inGraph) throws Exception {
		/* Represents the Minimum Spanning Tree resulting from the Prim's Algorithm */
		final WeightedGraph outGraph = new WeightedGraph();

		/* QueueMember is a wrapper class for the <end Vertex, weight> pair which defines an edge from another vertex */
		final QueueMember.QueCompare comparator = new QueCompare();

		/* Priority Queue for storing QueueMembers based upon lowest priority(edge weight) */
		final PriorityQueue<QueueMember> priQue = new PriorityQueue<QueueMember>(inGraph.getNumVertices(), comparator);

		/* map of end vertices to QueueMembers to correspond between PriorityQueue and input graph's vertex keyset */
		final Map<Vertex, QueueMember> pqVertexMap = new HashMap<Vertex, QueueMember>();

		/* handle empty graph case */
		if (inGraph.getNumVertices() == 0)
			return outGraph;

		/* technical beginning of prim's algorithm */

		/* pick a random vertex to start from and add it to the graph */
		final Vertex startVertex = inGraph.getStartingVertex();
		outGraph.addVertex(startVertex);

		/* put all starting vertex out going edges in the priority queue
		 * as well as in vertex mapping to stay in sync */
		for (Map.Entry<Vertex, Integer> outGoingEdge : inGraph.getAdjacentEdges(startVertex).entrySet()) {
			/* add each end vertex and QueueMember to map */
			final QueueMember tempOutEdge = new QueueMember(outGoingEdge.getKey(), outGoingEdge.getValue());
			pqVertexMap.put(outGoingEdge.getKey(), tempOutEdge);
			priQue.add(tempOutEdge);
		}

		for (int i = 1; i < inGraph.getNumVertices(); i++) {
			/* pop a vertex off the priority queue, on the first iteration,
			 * this is the end vertex to startVertex above, with minimum edge weight */
			final Vertex poppedVertex = priQue.remove().getVertex();

			/* based on the vertices already added to outGraph, and the edge weights of corresponding out going edge
			 * we find the vertex already in the outGraph which has the least weight from poppedVertex */
			final Vertex mstVertex = findClosestVertex(inGraph, outGraph, poppedVertex);

			/* only need to add poppedVertex since mstVertex is already in the outGraph */
			outGraph.addVertex(poppedVertex);
			outGraph.addEdge(poppedVertex, mstVertex, inGraph.edgeWeight(poppedVertex, mstVertex));

			/* adds all outgoing edges (vertex, weight QueueMembers) to the Priority Queue */
			enqueueOutEdges(poppedVertex, inGraph, outGraph, priQue, pqVertexMap);
		}

		return outGraph;
	}

	/**
	 * Searches for closest Vertex to a given Vertex
	 *
	 * @param inGraph graph which the MST is being determined for
	 * @param outGraph existing MST so far
	 * @param a Vertex which want to find the closest vertex to
	 * @return closest Vertex to {@code a} based on edge weight
	 * @throws Exception
	 */
	private static Vertex findClosestVertex(WeightedGraph inGraph, WeightedGraph outGraph, Vertex a) throws Exception {
		Vertex closest = null;
		Integer closestWeight = Integer.MAX_VALUE;

		for (Map.Entry<Vertex, Integer> outGoingEdge : inGraph.getAdjacentEdges(a).entrySet()) {
			/* if out going edge's vertex isn't in result graph, move on */
			if (!outGraph.hasVertex(outGoingEdge.getKey()))
				continue;
			/* if out going edge's weight >= prev weight, move on */
			if (outGoingEdge.getValue() >= closestWeight)
				continue;
			/* update best vertex */
			closest = outGoingEdge.getKey();
			closestWeight = outGoingEdge.getValue();
		}
		return closest;
	}

	/**
	 * Puts all out going edges from {@code v} to vertices not already in outGraph into the Priority Queue
	 *
	 * @param v Vertex to find outgoing edges from
	 * @param inGraph graph which the MST is being determined for
	 * @param outGraph existing MST so far
	 * @param pq which QueueMembers are added to if they're vertice's do not belong to outGraph
	 * @param pqMap mapping to correspond between inGraph and Priority Queue (keyed by Vertex)
	 * @throws Exception
	 */
	private static void enqueueOutEdges(Vertex v, WeightedGraph inGraph, WeightedGraph outGraph, PriorityQueue<QueueMember> pq, Map<Vertex, QueueMember> pqMap) throws Exception {
		for (Map.Entry<Vertex, Integer> outGoingEdge : inGraph.getAdjacentEdges(v).entrySet()) {
			/* we're only concerned with out going edges to vertices not already in the result MST */
			if (!outGraph.hasVertex(outGoingEdge.getKey())) {
				/* <Vertex, weight> not in priority queue vertex mapping, add it */
				if (!pqMap.containsKey(outGoingEdge.getKey())) {
					final QueueMember tempOutEdge = new QueueMember(outGoingEdge.getKey(), outGoingEdge.getValue());
					pqMap.put(outGoingEdge.getKey(), tempOutEdge);
					pq.add(tempOutEdge);

				} else if (pqMap.get(outGoingEdge.getKey()).getPriority() > outGoingEdge.getValue()) {
					/* out going vertex already exists in PQ and PQMapping, but this outgoing vertex/edge has a better weight */
					pq.remove(pqMap.get(outGoingEdge.getKey()));
					pq.offer(new QueueMember(outGoingEdge.getKey(), outGoingEdge.getValue()));
				}
			}
		}
	}

}