/**
 * @author Daniel DeCola
 * @email ddecola@gmail.com
 */
package com.graphproject.primsimpl;
/**
 * Vertex object used in graph structure, can be anything really, here it's just a wrapper
 * around a string value, but can be easily expanded on without additional changes to WeightedGraph.
 */
public class Vertex {
	private String id;

	public Vertex(String id) {
		this.setId(id);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return getId();
	}

	public boolean equals(Vertex v)	{
		return getId().equals(v.getId());
	}
}